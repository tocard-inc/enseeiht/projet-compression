import numpy as np

import obja.obja as obja


class Edge:
    """Edge representation"""

    def __init__(self, a: int, b: int) -> None:
        """Create an edge

        Args:
            a (int): first vertex
            b (int): second vertex
        """
        self.a = min(a, b)
        self.b = max(a, b)

        self.face1: Face | None = None
        self.face2: Face | None = None

        self.fold: float = 0.0
        self.curvature: float = 0.0

    def __eq__(self, __o: object) -> bool:
        """Check if two edges are equal

        Args:
            __o (object): other edge

        Returns:
            bool: True if equal, False otherwise
        """
        if isinstance(__o, Edge):
            return self.a == __o.a and self.b == __o.b

        return False


class Face:
    """Face representation"""

    def __init__(self, a: int, b: int, c: int) -> None:
        """Face constructor

        Args:
            a (int): first vertex index
            b (int): second vertex index
            c (int): third vertex index
        """
        self.a = a
        self.b = b
        self.c = c

        self.edges: list[Edge] = []
        self.a = a
        self.b = b
        self.c = c

        self.normal = np.zeros(3)

    def to_obja(self) -> obja.Face:
        """Convert face to obja format

        Returns:
            obja.Face: face in obja format
        """
        return obja.Face(self.a, self.b, self.c)

    def __eq__(self, __o: object) -> bool:
        """Check if two faces are equal

        Args:
            __o (object): other face

        Returns:
            bool: True if equal, False otherwise
        """
        if isinstance(__o, Face):
            return set((__o.a, __o.b, __o.c)) == set((self.a, self.b, self.c))

        return False


class Vertex:
    """Vertex representation"""

    def __init__(self, pos: np.ndarray[int, float]) -> None:
        """Vertex constructor

        Args:
            pos (np.ndarray[int, float]): position of the vertex
        """
        self.pos = pos

        self.vertex_ring: list[int] = []
        self.face_ring: list[int] = []

        self.normal: np.ndarray = np.zeros(3)

        self.area: float = 0.0
        self.curvature: float = 0.0

        self.border: bool = False

    def to_obja(self) -> np.ndarray:
        """Convert vertex to obja format

        Returns:
            np.ndarray: vertex in obja format
        """
        return self.pos


if __name__ == "__main__":
    pass

import argparse

from maps import MAPS


def main(args: argparse.Namespace) -> None:
    """Run MAPS model compression

    Args:
        args (Namespace): arguments (input and output path)
    """
    model = MAPS()
    model.parse_file(args.input)

    with open(args.output, 'w') as output:
        model.compress(output, args.level,
                       args.final or args.debug, args.debug)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=str, required=True)
    parser.add_argument('-o', '--output', type=str, required=True)
    parser.add_argument('-l', '--level', type=int, required=True)
    parser.add_argument('-f', '--final', type=bool, default=False)
    parser.add_argument('-d', '--debug', type=bool, default=False)
    args = parser.parse_args()

    main(args)

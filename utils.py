import numpy as np


def cot(x: float) -> float:
    """Cotangeante of x

    Args:
        x (float): angle

    Returns:
        float: cotangeante of x
    """
    sin_x = np.sin(x)
    if sin_x == 0:
        return 1e16
    return np.cos(x) / sin_x


def sliding_window(l: list, n: int = 2) -> list[tuple]:
    """Create a sliding window of size n

    Args:
        l (list): list to create the sliding window from
        n (int, optional): number of value. Defaults to 2.

    Returns:
        list[tuple]: sliding window
    """
    k = n - 1
    l2 = l + [l[i] for i in range(k)]
    res = [(x for x in l2[i:i+n]) for i in range(len(l2)-k)]
    return res


if __name__ == '__main__':
    pass
